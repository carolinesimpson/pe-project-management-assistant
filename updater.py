

class Updater(object):

    def __init__(self, milestone):
        self.milestone = milestone

    def update_planning_issue(self):
        # TODO: write the actual list of deliverables and stretch issues back into the planning issue
        # write between tags in the planning issue description -> overwrite if rerun.
        # if tags do not exist, then add a comment instead.
        pass

    def _clean_up_stretch_labels(self):
        # TODO: Remove all stretch labels
        # This should only be run after issues are moved around pre_start


        pass

    def update_planning_issue_with_likely_to_rollover(self):
        # TODO: write list of issues likely to rollover to next milestone into planning issue
        # write between tags in the planning issue description -> overwrite if rerun.
        # if tags do not exist, then add a comment instead.
        pass

    def shift_rollover_milestone(self):
        # TODO: if deliverable issues are not complete and at risk in current milestone, move to next as Deliverabble
        # TODO: if stretch issues are not complete and at risk, move to next as Deliverable
        # TODO: if other issues are not complete and at risk, move to backlog
        pass

    def activate_issues(self):
        # TODO: implement
        # if issue is cicd::planning
        #   if issue has weight
        #      if workflow::planning breakdown
        #          set workflow::ready for development
        #      if issue is Verify::P1
        #         set milestone to new milestone
        #      if issue is Verify::P2
        #         set to next milestone
        #      if issue is Verify::P3
        #         set to milestone after next
        #      set cicd::active
        #   if weight not set
        #      if issue is Verify::P2
        #         set Verify::P1
        #      if issue is Verify::P3
        #         set Verify::P2

        pass

    def pre_start(self):
        self.shift_rollover_milestone()
        self._clean_up_stretch_labels()
        self.activate_issues()



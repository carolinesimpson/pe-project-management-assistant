import os
import json
import logging
from collections import defaultdict

import numpy
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from mdutils.mdutils import MdUtils

from helper import add_estimate_discussion_note, get_project_issues


# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def read_embeddings(embedding_files_subset):
    """
    Read embeddings and metadata from JSON files in the 'embeddings' directory.

    Returns:
    tuple: A tuple containing:
        - embeddings (numpy.ndarray): Array of embeddings.
        - titles (list of str): List of issue titles.
        - web_urls (list of str): List of issue URLs.
    """
    issues_dir = 'embeddings'
    embeddings_subset, embeddings_subset_meta, embeddings_remainder, embeddings_remainder_meta = [], [], [], []
    for filename in os.listdir(issues_dir):
        if filename.endswith('.json'):
            file_path = os.path.join(issues_dir, filename)
            try:
                with open(file_path, 'r', encoding='utf-8') as file:
                    issue_data = json.load(file)

                    if filename in embedding_files_subset:
                        embeddings_subset.append(issue_data['embedding'])
                        embeddings_subset_meta.append(
                            {
                                "id": issue_data['id'],
                                "project_id": filename.split('-')[0],
                                "iid": issue_data['web_url'].split('/')[-1],
                                "title": issue_data['title'],
                                "web_url": issue_data['web_url'],
                                "weight": issue_data['weight'],
                                "days": issue_data['days'],
                            }
                        )
                    else:
                        embeddings_remainder.append(issue_data['embedding'])
                        embeddings_remainder_meta.append(
                            {
                                "id": issue_data['id'],
                                "project_id": filename.split('-')[0],
                                "iid": issue_data['web_url'].split('/')[-1],
                                "title": issue_data['title'],
                                "web_url": issue_data['web_url'],
                                "weight": issue_data['weight'],
                                "days": issue_data['days'],
                            }
                        )

            except Exception as e:
                logging.error("Failed to read file %s: %s", filename, e)

    embeddings_subset = np.array(embeddings_subset)
    embeddings_remainder = np.array(embeddings_remainder)
    return embeddings_subset, embeddings_subset_meta, embeddings_remainder, embeddings_remainder_meta


def generate_estimates(threshold, embedding_files_subset, update_issues=True):
    """
    Identifies potential similar issues based on cosine similarity of embeddings.

    Parameters:
    threshold (float): The cosine similarity threshold to consider two issues as duplicates.

    Returns:
    None
    """
    (embeddings_subset, embeddings_subset_meta,
     embeddings_remainder, embeddings_remainder_meta) = read_embeddings(embedding_files_subset)

    if embeddings_remainder.size == 0 or embeddings_subset.size == 0:
        logging.info("No embeddings to process.")
        return

    similarity_matrix = cosine_similarity(X=embeddings_subset, Y=embeddings_remainder)
    n_subsest_issues = len(embeddings_subset)
    n_remainder_issues = len(embeddings_remainder)
    similar_issues = []

    for i in range(n_subsest_issues):
        for j in range(n_remainder_issues):
            if similarity_matrix[i, j] >= threshold:
                similar_issues.append((i, j, similarity_matrix[i, j]))

    similar_issues.sort(key=lambda x: x[2], reverse=True)

    md_file = MdUtils(file_name='estimates', title='Issue Estimates')

    list_of_strings = ["Title", "Web URL", "Manual Weight", "Spent Days", "MRs", "Estimated Weight", "Estimated Days", "Estimated MRs", "Based On"]
    n_columns = len(list_of_strings)

    issue_days = defaultdict(list)
    issue_weights = defaultdict(list)
    issue_simular = defaultdict(list)
    issue_mrs = defaultdict(list)
    for i, j, sim in similar_issues:
        issue_mrs[i].append(len(embeddings_remainder_meta[j]["days"]))
        issue_days[i].extend(embeddings_remainder_meta[j]["days"] or [1])
        issue_weights[i].append(int(embeddings_remainder_meta[j]["weight"] or 1))
        issue_simular[i].append(j)

        logging.info("Issues found with high similarity score: %.2f", sim)
        logging.info("- %s - %s - %s - %s",
                     embeddings_subset_meta[i]["title"],
                     embeddings_subset_meta[i]["web_url"],
                     embeddings_subset_meta[i]["weight"],
                     embeddings_subset_meta[i]["days"])
        logging.info("- %s - %s - %s - %s",
                     embeddings_remainder_meta[j]["title"],
                     embeddings_remainder_meta[j]["web_url"],
                     embeddings_remainder_meta[j]["weight"],
                     embeddings_remainder_meta[j]["days"]
                     )

    for i, meta in enumerate(embeddings_subset_meta):
        logging.info("- %s - %s - estimated days: %s - estimated weight: %s", meta["title"],
                     meta["web_url"], np.median(issue_days[i]), np.mean(issue_weights[i]))
        logging.info("- %s", issue_simular[i])

        similar_issues_list = []
        based_on_list = []
        for x in issue_simular[i]:
            similar_issues_list.append(embeddings_remainder_meta[x])
            link = md_file.new_inline_link(
                link=embeddings_remainder_meta[x]["web_url"],
                text=embeddings_remainder_meta[x]["title"])
            based_on_list.append(link)

        based_on = ", ".join(based_on_list)

        estimate_weight = np.ceil(np.mean(issue_weights[i]))
        estimate_days = np.ceil(np.median(issue_days[i]))
        estimate_mrs = np.ceil(np.mean(issue_mrs[i]))

        list_of_strings.extend(
            [meta["title"],
             meta["web_url"],
             meta["weight"],
             np.sum(meta["days"]),
             len(meta["days"]),
             estimate_weight,
             estimate_days,
             estimate_mrs,
             based_on
             ]
        )

        if update_issues:
            update_issue_with_estimate(meta["iid"], meta["project_id"],
                                       estimate_weight, estimate_days, estimate_mrs, similar_issues_list)

    md_file.new_table(columns=n_columns, rows=len(embeddings_subset_meta) + 1,
                      text=list_of_strings, text_align='center')

    md_file.create_md_file()


def update_issue_with_estimate(issue_id, project_id,  est_weight, est_days, est_mrs, based_on):

    # temp override for testing
    # project_id = "57849545"
    # issue_id = "1"

    if numpy.isnan(est_weight):
        body = "Unable to generate estimates. Unable to determine similar issues."
    else:

        body = f"""
Estimated weight: {est_weight}
Estimated days for development: {est_days}
Estimated number of MRs: {est_mrs}

Estimates are generated based on historical work effort from issues that may be similar. The issues that were used
to generate this data were as follows. \n\n"""

        for meta in based_on:
            body += f" * {meta['web_url']}+ \n"

    add_estimate_discussion_note(issue_id, project_id, body)

# if __name__ == "__main__":
    # find_similar(0.9)

    # discussion_id, note_id = find_or_create_estimate_discussion_id(issue)
    #
    # add_estimate_discussion_note(issue, discussion_id, note_id, "This is some info about the estimate")
    # print(discussion_id, note_id)
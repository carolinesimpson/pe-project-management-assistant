from typing import List

from vertexai.language_models import TextEmbeddingInput, TextEmbeddingModel
from summarizer import Summarizer
from pathlib import Path

import logging
import json

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


def embed_text(
        texts=None,
        task: str = "RETRIEVAL_DOCUMENT",
        model_name: str = "textembedding-gecko@003",
) -> List[List[float]]:
    """Embeds texts with a pre-trained, foundational model."""
    if texts is None:
        return [[]]
    model = TextEmbeddingModel.from_pretrained(model_name)
    inputs = [TextEmbeddingInput(text, task) for text in texts]
    embeddings = model.get_embeddings(inputs)
    return [embedding.values for embedding in embeddings]


def create_embeddings(flat_issues):
    """
    Retrieve issues and generate embeddings for each, saving them as JSON files.
    """
    summarizer = Summarizer()
    # matching_issues = gitlab_issues.get_issues_with_label("group::code creation")
    total_issues = len(flat_issues)

    for index, issue in enumerate(flat_issues):
        # issue_id, project_id = issue['issue_id'], issue['project_id']
        # title, web_url = issue['title'], issue['web_url']

        logging.info(f"Processing ({index + 1} of {total_issues}) - {issue['issue']['title']}")

        # text = gitlab_issues.get_issue_summary(project_id, issue_id)
        summary = summarizer.summarize(issue["flat"]) or issue["flat"]
        embedding = embed_text(texts=[summary])[0]

        save_embedding(issue, embedding)


def save_embedding(flat_issue, embedding):
    """
    Save the embedding to a JSON file.
    """
    project_id = flat_issue["issue"]["project_id"]
    issue_id = flat_issue["issue"]["id"]
    title = flat_issue["issue"]["title"]
    web_url = flat_issue["issue"]["web_url"]
    weight = flat_issue["issue"]["weight"]
    days = flat_issue["days"]
    summary = flat_issue["flat"]

    file_contents = {
        "id": issue_id,
        "title": title,
        "web_url": web_url,
        "weight": weight,
        "days": days,
        "summary": summary,
        "embedding": embedding
    }

    Path("embeddings").mkdir(parents=True, exist_ok=True)
    file_path = f'embeddings/{project_id}-{issue_id}.json'

    with open(file_path, 'w') as f:
        json.dump(file_contents, f, indent=4)


if __name__ == "__main__":
    embed = embed_text()
    print(embed)

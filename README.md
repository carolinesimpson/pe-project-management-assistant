# Pipeline Execution Project Management Assistant

## Description
A suite of python functions that allow automated updates of issues according to our project management processes. 

## Usage

```python
from updater import Updater

auto = Updater("16.9")

# This can be run within the last 2 weeks of the development milestone 
auto.update_planning_issue_with_likely_to_rollover()

# This should be run about 1 week before the end of the milestone to allow for deliverables to be marked
auto.pre_start()

# manually adjust ~Deliverable and ~Stretch labels for the upcoming milestone according to capacity

# This should be run after setting Deliverable labels, but can be run multiple times (overwrites content)
auto.update_planning_issue()

```

## License
[MIT License](LICENSE) 

## Project status
Project under initial development - really only intended for internal team use, since it follows the PE team process.
import argparse
import datetime
import os
import requests


gitlab_url = "https://gitlab.com/"
group_id = os.getenv("GROUP_ID")


def get_headers():
    # Set the headers for the API request
    personal_access_token = os.getenv("TOKEN")
    headers = {
        "Private-Token": personal_access_token
    }
    return headers


def get_user_id(username):
    headers = get_headers()
    url = f"{gitlab_url}api/v4/users?username={username}"

    response = requests.get(
        url,
        headers=headers
    )

    if response.status_code != 200:
        raise Exception("Error getting issues")

    users = response.json()

    for user in users:
        return user.get("id")


def get_paginated_issues(url_function):
    headers = get_headers()
    # Make a GET request to get a list of all the issues in the project

    page = 1
    total_pages = 10
    issues = []

    while page < total_pages:

        response = requests.get(
            url_function(page),
            headers=headers
        )

        response_headers = response.headers

        if response.status_code != 200:
            raise Exception(f"Error getting items: {response.status_code} - {response.reason} - {response.url}")

        if not response.json():
            break

        # Convert the response to a JSON object
        issues.extend(response.json())

        if "x-total-pages" in response_headers:
            total_pages = int(response_headers.get("x-total-pages")) + 1

        print(f"Got {page} of {total_pages} ")
        # page = int(response_headers.get("x-next-page"))

        page += 1
        print(f"Next is {page} of {total_pages} ")

    return issues


def get_milestone_issues(milestone, confidential="true", state="closed", labels="group::pipeline%20execution"):

    def get_url(page):
        return (f"{gitlab_url}api/v4/groups/9970/issues?labels={labels}"
                f"&milestone={milestone}&state={state}&confidential={confidential}&scope=all&page={page}")

    return get_paginated_issues(
        get_url
    )

def get_project_issues(project_id):
    def get_url(page):
        return (f"{gitlab_url}api/v4/projects/{project_id}/issues?page={page}")

    return get_paginated_issues(
        get_url
    )

def get_issue_comments(issue):
    def get_url(page):
        return issue["_links"]["notes"] + f"?page={page}"


    notes = get_paginated_issues(
        get_url
    )

    return [{'text': c['body'], 'author': c['author']['name']} for c in notes]


def find_or_create_estimate_discussion_id(issue_id, project_id):
    def get_url(page):
        return f"{gitlab_url}api/v4/projects/{project_id}/issues/{issue_id}/discussions/?page={page}"

    discussion_threads = get_paginated_issues(
        get_url
    )

    thread_text = "## :crystal_ball: Experimental AI Estimation Thread"

    for thread in discussion_threads:
        for note in thread["notes"]:
            if note["body"] == thread_text:
                return thread["id"], note["id"]
    else:
        headers = get_headers()
        params = {
            "body": thread_text,
            "id": project_id,
            "issue_iid": issue_id,
        }
        response = requests.post(
            f"{gitlab_url}api/v4/projects/{project_id}/issues/{issue_id}/discussions",
            data=params,
            headers=headers
        )

        if response.status_code == 201 or response.status_code == 200:
            thread = response.json()
            return thread["id"], thread["notes"][0]["id"]
        else:
            raise Exception("Error creating estimate discussion thread")


def add_estimate_discussion_note(issue_id, project_id, body):

    discussion_id, note_id = find_or_create_estimate_discussion_id(issue_id, project_id)
    headers = get_headers()
    params = {
        "body": body,
        "discussion_id": discussion_id,
        "id": project_id,
        "issue_iid": issue_id,
        "note_id": note_id,

    }
    response = requests.post(
        f"{gitlab_url}api/v4/projects/{project_id}/issues/{issue_id}/discussions/{discussion_id}/notes",
        data=params,
        headers=headers
    )


def get_issue_mr_times(issue):
    def get_url(page):
        return f"{gitlab_url}api/v4/projects/{issue['project_id']}/issues/{issue['iid']}/related_merge_requests/?page={page}"

    mrs = get_paginated_issues(
        get_url
    )

    mr_days = []
    for mr in mrs:
        start = mr["created_at"]
        end = mr["closed_at"] or mr["merged_at"]

        st = datetime.datetime.strptime(start, "%Y-%m-%dT%H:%M:%S.%fZ")
        if end:
            et = datetime.datetime.strptime(end, "%Y-%m-%dT%H:%M:%S.%fZ")
        else:
            et = datetime.datetime.now()
        mr_days.append((et - st).days)

    return mr_days


def flatten_issues(issues):
    flattened_issues = []
    for issue in issues:
        """Summarize the title, description and comments of a GitLab issue into a single string"""
        if not issue:
            continue

        lines = []
        lines.append(f"# Title: {issue['title']}")
        lines.append("----------")
        lines.append("# Description")
        lines.append(issue['description'])
        lines.append("----------")
        lines.append("# Comments")
        for comment in get_issue_comments(issue):
            lines.append(f"{comment['author']}: {comment['text']}")

        mr_days = get_issue_mr_times(issue)
        flattened_issues.append(
            {
                "flat": "\n".join(lines),
                "days": mr_days,
                "issue": issue,
            }
        )

    return flattened_issues



def get_mrs(author=None, reviewer=None, approver_id=None, start_date=None, end_date=None):
    api_url = f"{gitlab_url}api/v4//merge_requests?"
    params = ["scope=all", "confidential=true"]
    if author:
        params.append(f"author_username={author}")
    if reviewer:
        params.append(f"reviewer_username={reviewer}")
    if approver_id:
        params.append(f"approved_by_ids[]={approver_id}")
    if start_date:
        params.append(f"created_after={start_date}")
    if end_date:
        params.append(f"created_before={end_date}")

    api_url += "&".join(params)

    def get_url(page):
        return f"{api_url}&page={page}"

    return get_paginated_issues(
        get_url
    )


# if __name__ == '__main__':
    # create_daily_issues()
    # get_milestone_issues("16.4")
    # issues = get_project_issues("57849545")
    #
    # issue = issues[0]
    # discussion_id, note_id = find_or_create_estimate_discussion_id(issue)
    #
    # add_estimate_discussion_note(issue, discussion_id, note_id, "This is some info about the estimate")
    # print(discussion_id, note_id)

import itertools

from issue_embeddings import create_embeddings
from estimator import generate_estimates

from helper import get_milestone_issues, flatten_issues

import os
import vertexai


def load_embeddings():
    milestone_major = ["16", "15", "14", "13", "12"]
    milestone_minor = [".0", ".1", ".2", ".3", ".4", ".5", ".6", ".7", ".8", ".9", ".10", ".11"]
    milestones = [f"{x[0]}{x[1]}" for x in itertools.product(milestone_major, milestone_minor)]
    milestones.append("17.0")
    milestones.reverse()
    for milestone in milestones:
        print("Loading embeddings for milestone {}".format(milestone))
        closed = get_milestone_issues(milestone, confidential="false", labels="group::pipeline%20execution")
        flattened = flatten_issues(closed)
        create_embeddings(flattened)


if __name__ == '__main__':
    project_id = os.getenv("GCP_PROJECT")
    vertexai.init(project=project_id, location="us-central1")

    # load_embeddings()
    upcoming_17_1 = get_milestone_issues("17.1", confidential="false", state="opened")
    # # flattened = flatten_issues(upcoming_17_1)
    # # create_embeddings(flattened)
    #
    embeddings_subset = [f"{x['project_id']}-{x['id']}.json" for x in upcoming_17_1]
    #
    generate_estimates(0.8, embeddings_subset, update_issues=False)
    # #




import logging
from vertexai.generative_models import GenerativeModel, Part


class Summarizer(object):
    def __init__(self):
        self.model = GenerativeModel(model_name="gemini-1.0-pro")

    def summarize(self, issue_text):
        try:
            response = self.model.generate_content(
                [
                    Part.from_text(f"Summarize the following text: {issue_text}")
                ]
            )
            return response.text
        except Exception as e:
            logging.error(e)


